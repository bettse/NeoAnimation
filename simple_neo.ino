#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

#define PIN            9
#define NUMPIXELS      24

struct Color {
  int red;
  int green;
  int blue;
};

struct RotateData {
  unsigned long previousMillis;
  long interval;
};

struct RandomData {
  int currentPixel;
  Color color;
  unsigned long previousMillis;
  long interval;
};

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

RotateData rotateData;
RotateData smearData;
RandomData randomData;

void setup() {  
  randomSeed(analogRead(0));

  pixels.begin(); // This initializes the NeoPixel library.
  pixels.setBrightness(127);

  randomData.currentPixel = 0;
  randomData.color.red = 127;
  randomData.color.green = 0;
  randomData.color.blue = 127;
  randomData.interval = 250;
  randomData.previousMillis = 0;

  rotateData.previousMillis = 0;
  rotateData.interval = 6000/pixels.numPixels();

  smearData.previousMillis = 0;
  smearData.interval = 3000/pixels.numPixels();

  colorWheel();
  //Set one
  //pixels.setPixelColor(0, pixels.Color(255, 255, 255));
  //pixels.show();

}

void loop() {
  //randomOn();
  //smear();
  rotate();
}

void colorWheel() {
  int max = pixels.numPixels(); //pre-compute
  int step = max * 2 / 3;
  for(int i = 0; i < max; i++) {
    int red = scale16((step*0 + i) % max);
    int grn = scale16((step*1 + i) % max);
    int blu = scale16((step*2 + i) % max);
    pixels.setPixelColor(i, pixels.Color(red, grn, blu));
  }
  pixels.show();
}

int scale16(int x) {
  return min(max(0, -4 * x * x + 64 * x + 0), 255);
}

void smear() {
  //Assume at least one pixel is non-zero
  unsigned long currentMillis = millis();
  if (currentMillis - smearData.previousMillis >= smearData.interval) {
    smearData.previousMillis = currentMillis;
    int max = pixels.numPixels(); //pre-compute

    for(int i = 0; i < max; i++) {
      uint32_t nextColor = pixels.getPixelColor((i + 1) % max);
      if (nextColor > 0) {
        pixels.setPixelColor(i, nextColor);
        pixels.show();
      }
    }
  }
}

void rotate() {
  unsigned long currentMillis = millis();
  if (currentMillis - rotateData.previousMillis >= rotateData.interval) {
    rotateData.previousMillis = currentMillis;
    int max = pixels.numPixels(); //pre-compute

    //Save off the existing colors to prevent loss
    uint32_t colors[NUMPIXELS] = {0};
    for(int i = 0; i < max; i++) {      
      colors[i] = pixels.getPixelColor(i);      
    }
    
    for(int i = 0; i < max; i++) {      
      pixels.setPixelColor(i, colors[(i + 1) % max]);
    } 
    pixels.show();   
  }
}

void randomOn() {
  unsigned long currentMillis = millis();
  if (currentMillis - randomData.previousMillis >= randomData.interval) {
    randomData.previousMillis = currentMillis;
    pixels.setPixelColor(randomData.currentPixel, pixels.Color(0, 0, 0));
    randomData.currentPixel = random(0, pixels.numPixels());
    pixels.setPixelColor(randomData.currentPixel, pixels.Color(randomData.color.red, randomData.color.green, randomData.color.blue));
    pixels.show();
  }
}

